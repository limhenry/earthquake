

function main() {
    $(function() {
        $.ajax({
            url: 'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson',
            method: 'GET',
            dataType: 'json',
            jsonpCallback: 'haha',
            error: function(err) {
                Materialize.toast('I am a toast!', 4000);
            },
            success: function(data) {
                datas = data;
                Materialize.toast('Updated!', 4000);
                parse();
                
                
            }
        });
    });
};

function parse() {
    $('#myList').empty();
    var i = 0;
    console.log(i);
    for (i = i; i < 20; i++) {
        var card = document.createElement("div");
        card.setAttribute("class", "card-panel");
        card.setAttribute("id", "card-panel");
        var mag = document.createElement("h4");
        var place = document.createElement("p");
        var time = document.createElement("p");
        var location = document.createElement("a");
        var magText = document.createTextNode(datas.features[i].properties.mag);
        var placeText = document.createTextNode(datas.features[i].properties.place);
        var locationText = document.createTextNode("maps");
        var timeText = document.createTextNode(new Date(datas.features[i].properties.time));



        var po = document.createTextNode("https://www.google.com/maps?q=" + locationText[1] + "," + locationText[0]);

        if (datas.features[i].properties.mag > 7) {
            mag.appendChild(magText);
            mag.setAttribute("style", "color:#D50000");
        } else if (datas.features[i].properties.mag > 6) {
            mag.appendChild(magText);
            mag.setAttribute("style", "color:#F44336");
        } else if (datas.features[i].properties.mag > 5) {
            mag.appendChild(magText);
            mag.setAttribute("style", "color:#FF9800");
        } else if (datas.features[i].properties.mag > 4) {
            mag.appendChild(magText);
            mag.setAttribute("style", "color:#FFC107");
        } else if (datas.features[i].properties.mag > 3) {
            mag.appendChild(magText);
            mag.setAttribute("style", "color:#FFEB3B");
        } else if (datas.features[i].properties.mag > 2) {
            mag.appendChild(magText);
            mag.setAttribute("style", "color:#CDDC39");
        } else if (datas.features[i].properties.mag > 1) {
            mag.appendChild(magText);
            mag.setAttribute("style", "color:#4CAF50");
        } else {
            continue;
        }
        place.appendChild(placeText);
        location.appendChild(locationText);
        time.appendChild(timeText);
        card.appendChild(mag);
        card.appendChild(place);
        card.appendChild(time);
        var point = String(datas.features[i].geometry.coordinates).split(",");
        location.setAttribute("href", "https://www.google.com/maps?q=" + point[1] + "," + point[0]);
        card.appendChild(location);

        document.getElementById("myList").appendChild(card);
        console.log("hahah")
    }
}


main();